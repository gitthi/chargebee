package leaftapplayground;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;



public class Annotation {

	public  ChromeDriver driver;
	public String excelfilename;

	
	@BeforeMethod
	public void login() {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://app.chargebee.com/login");
	
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
			}
	
	
	

@DataProvider(name ="fetch")
public String[][] getData() throws InvalidFormatException, IOException {
	return Readex.Readmyexcel(excelfilename);
}

	
}
