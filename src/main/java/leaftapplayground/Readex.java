package leaftapplayground;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Readex {

	public static String[][] Readmyexcel(String filename) throws InvalidFormatException, IOException {
		// TODO Auto-generated method stub
		
		//file location and navigate to workbook
		
		XSSFWorkbook workbook = new XSSFWorkbook("./data/"+filename+".xlsx");
		
		//navigate to worksheet
		
		XSSFSheet worksheet = workbook.getSheetAt(0); // if you know the index of particular sheet getSheetAt(index)
		                                                    //getSheet("sheet1") also use it to mention the particular sheet name
		
		//Determine no of rows 
		
        //int rowcount = worksheet.getPhysicalNumberOfRows(); //to ignore the column header use getLastRowNum()
		int rowcount = worksheet.getLastRowNum();
		System.out.println("No of Row:" +rowcount);
		
		//Determine no of cells
		int cellcount = worksheet.getRow(0).getLastCellNum();
		System.out.println("No of Cell:" +cellcount);
		
		String[][] data = new String[rowcount][cellcount];
		
		//Iterate the row's and columns
		for(int i=1; i <= rowcount;i++) {
			
			XSSFRow row = worksheet.getRow(i);
			
		for(int j=0;j < cellcount;j++) {
			
			XSSFCell cell = row.getCell(j);
			
			data[i-1][j] = cell.getStringCellValue();
			System.out.println("Read Excel data:" +data[i-1][j]);
			
		}	
			
			
		}
//		workbook.close();
		return data;

}
}